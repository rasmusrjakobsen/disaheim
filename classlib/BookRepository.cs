namespace classlib;
public class BookRepository
{
    private List<Book> books = new List<Book>();

    public void AddBook(Book book)
    {
        books.Add(book);
    }
    public Book GetBook(string itemId)
    {
        foreach (var Book in books)
        {
            if (Book.ItemId == itemId)
            {
                return Book;
            }
        }

        return null;
    }
    public double GetTotalValue()
    {
        Utility utility = new Utility();
        double sum = 0;
        foreach (var book in books)
        {
            sum += utility.GetValueOfBook(book);
        }
        return sum;
    }
}
