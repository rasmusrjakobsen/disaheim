namespace classlib;
public class AmuletRepository
{
    private List<Amulet> amulets = new List<Amulet>();

    public void AddAmulet(Amulet amulet)
    {
        amulets.Add(amulet);
    }
    public Amulet GetAmulet(string itemId)
    {
        foreach (var amulet in amulets)
        {
            if (amulet.ItemId == itemId)
            {
                return amulet;
            }
        }
        return null;
    }
    public double GetTotalValue()
    {
        Utility utility = new Utility();
        double sum = 0;
        foreach (var amulet in amulets)
        {
            sum += utility.GetValueOfAmulet(amulet);
        }
        return sum;
    }

}