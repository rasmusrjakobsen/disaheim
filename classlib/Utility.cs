namespace classlib;
public class Utility
{
    public double LowQualityValue {get; set;} = 12.5;
    public double MediumQualityValue {get; set;} = 20.0;
    public double HighQualityValue {get; set;} = 27.5;
    public double CourseHourValue {get; set;} = 875;
    public double GetValueOfBook(Book book)
    {
        return book.Price;
    }
    public double GetValueOfAmulet(Amulet amulet)
    {
        if (amulet.Quality == Level.low)
        {
            return LowQualityValue;
        }
        else if (amulet.Quality == Level.medium)
        {
            return MediumQualityValue;
        }
        else
        {
            return HighQualityValue;
        }
    }

    public double GetValueOfMerchandise(Merchandise merchandise)
    {
        if (merchandise is Book book)
        {
            return book.Price;
        }
        else if (merchandise is Amulet amulet)
        {
            if (amulet.Quality == Level.low)
            {
                return LowQualityValue;
            }
            else if (amulet.Quality == Level.medium)
            {
                return MediumQualityValue;
            }
            else
            {
                return HighQualityValue;
            }
            
        }
        return 0;
    }

    public double GetValueOfCourse(Course course)
    {
        int num = course.DurationInMinutes / 60;
        int remainder = course.DurationInMinutes % 60;

        if (remainder != 0)
            return (num + 1) * 875;
        else 
            return num * CourseHourValue;
    }
}