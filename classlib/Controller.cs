namespace classlib;
public class Controller
{
    private List<Book> _books = new List<Book>();
    public List<Book> Books
    {
        get {return _books;}
        set {_books = value;}
    }
    private List<Amulet> _amulets = new List<Amulet>();
    public List<Amulet> Amulets
    {
        get {return _amulets;}
        set {_amulets = value;}
    }
    private List<Course> _courses = new List<Course>();
    public List<Course> Courses
    {
        get {return _courses;}
        set {_courses = value;}
    }

    public Controller (List<Book> books, List<Amulet> amulets)
    {
        Books = books;
        Amulets = amulets;
    }
    public Controller()
    {
        
    }

    public void AddToList(Book book)
    {
        Books.Add(book);
    }
    public void AddToList(Amulet amulet)
    {
        Amulets.Add(amulet);
    }
    public void AddToList(Course course)
    {
        Courses.Add(course);
    }
}