namespace classlib;
public class CourseRepository
{
    private List<Course> courses = new List<Course>();

    public void AddCourse(Course course)
    {
        courses.Add(course);
    }
    public Course GetCourse(string name)
    {
        foreach (var course in courses)
        {
            if (course.Name == name)
            {
                return course;
            }
        }
        return null;
    }
    public double GetTotalValue()
    {
        Utility utility = new Utility();
        double sum = 0;
        foreach (var course in courses)
        {
            sum += utility.GetValueOfCourse(course);
        }
        return sum;
    }
}