namespace classlib;
public class MerchandiseRepository
{
    private List<Merchandise> merchandises = new List<Merchandise>();

    public void AddMerchandise(Merchandise merchandise)
    {
        merchandises.Add(merchandise);
    }
    public Merchandise GetMerchandise(string itemId)
    {
        foreach (var merchandise in merchandises)
        {
            if (merchandise.ItemId == itemId)
            {
                return merchandise;
            }
        }
        return null;
    }
    public double GetTotalValue()
    {
        Utility utility = new Utility();
        double sum = 0;
        foreach (var merchandise in merchandises)
        {
            sum += utility.GetValueOfMerchandise(merchandise);
        }
        return sum;
    }



}